# -*- coding: utf-8 -*-
"""
Created on Wed May 13 11:24:46 2020

@author: niels
"""

#%%
import numpy as np;
import pandas as pd;
import seaborn as sns; sns.set(style="whitegrid")
import matplotlib.pyplot as plt;
from pylab import savefig;

#%%
# =============================================================================
# load data
# =============================================================================
data_path = "C:\\Users\\niels\\Documents\\Nanobiology\\BEP\\data\\";

data_file = "pwgs-sci-dpc_it25_50s_6l_2000g_0_4_4n_scifix\\";
parameter = "sample number";
parameter_scaling = lambda x : x + 1;

# parameter = "genome size";
# parameter_scaling = lambda x : x*250 + 1000;

pipe_names = {
    0 : "phylowgs",
    1 : "sciclone",
    2 : "dpclust"
    };

data_names = ["pipe_estimations.npy","true_values.npy"];



e = np.load(data_path + data_file + data_names[0], allow_pickle = True);
t = np.load(data_path + data_file + data_names[1], allow_pickle = True);

#test array
a = np.array(
      [[[2, 0],
        [1, 7],
        [3, 8]],

       [[5, 0],
        [0, 7],
        [8, 0]],

       [[2, 5],
        [8, 2],
        [1, 2]],

       [[5, 3],
        [1, 6],
        [3, 2]]])

def collapse_to_frame(a, names = None):
    dims = np.array(a.shape);
    datapoints = a.reshape(np.prod(dims[0:-1]),-1);
    
    prev_rep = 1;
    all_columns = datapoints
    for i in reversed(range(np.ndim(a)-1)):
        # print("i:",i);
        rep = prev_rep;
        # print("rep:",rep);
        rep_arr = np.repeat(np.arange(dims[i]), rep);
        # print("rep_arr:",rep_arr);
        # print("datapoints.shape[0]:",datapoints.shape[0]);
        # print("len(rep_arr)",len(rep_arr));
        new_column = np.array( list(rep_arr) * int(datapoints.shape[0]//len(rep_arr)) );
        # print("new_column:",new_column);
        all_columns = np.column_stack((new_column,all_columns))
        prev_rep = len(rep_arr);
    
    if names != None:
        df = pd.DataFrame(all_columns, columns = names);
    else:
        df = pd.DataFrame(all_columns);
    
    return (df);

print(e.shape);
print(t.shape);

# =============================================================================
# merge true data with the estimated data in dataframe
# =============================================================================

#expand the true values to fit the estimations dataframe
t_expand = np.empty_like(e[:,:,:,:,:-1]);

for i in range(e.shape[0]):
    for j in range(e.shape[1]):
        t_expand[i,j,:,:,:] = np.full((e.shape[2],e.shape[3],2),t[i,j,:]);

df_e = collapse_to_frame(e, names = ["pars","loops","pipes","subloops","subclone number","ccfs","run time"]);
df_t = collapse_to_frame(t_expand, names = ["pars","loops","pipes","subloops","subclone number true","ccfs true"]);

#merge
df = pd.merge(df_e,df_t,how = "left", on = ["pars","loops","pipes","subloops"]);

#%%============================================================================
# name and format the dataframe
# =============================================================================

for cat in ["pars","loops","pipes","subloops","subclone number","subclone number true"]:
    df[cat] = df[cat].astype(int);
df["run time"] = df["run time"].astype(float);

df["pipes"] = df["pipes"].replace(pipe_names.keys(),pipe_names.values());

df["pars"] = df["pars"].apply(parameter_scaling)

#tag rows with correct subclone number estimate
df["subclone number correct"] = (df["subclone number"] == df["subclone number true"]).astype(int);
df["estimation succesful"] = (df["subclone number"] != 0).astype(int);

#%%============================================================================
# plotting
# =============================================================================

ax = [None]*3;

#%%Average subclone number estimation
ax[0] = sns.lineplot(x="pars", y="subclone number", hue = "pipes", data = df);
ax[1] = sns.lineplot(x="pars", y="subclone number true", color = "red", label = "True", data = df);
plt.xlabel(parameter);
plt.title("Average subclone number of each tool (pipe) per simulation parameter (sample number)");

#%% Correct subclone number estimations
ax[0] = sns.lineplot(x="pars", y="subclone number correct", hue = "pipes", estimator = "sum", data = df);
plt.xlabel(parameter);
plt.title("Correct subclone number estimations of each tool (pipe) per simulation parameter (sample number)");

#%% Average run time (correct subclone)
ax[0] = sns.lineplot(x="pars", y="run time", hue = "pipes", estimator = "mean", data = df
                      , style = "subclone number correct");
plt.xlabel(parameter);
plt.ylabel("run time [s]")
plt.title("Run time of each tool (pipe) per simulation parameter (sample number)");

#%% Number of succesful runs and correct estimations of subclone number of pipes per loop
ax[0] = sns.barplot(x="loops", y="estimation succesful", hue = "pipes", data = df, estimator = np.sum, 
                    palette = sns.hls_palette(8, l=.3, s=.8), errwidth = 0);
ax[0] = sns.barplot(x="loops", y="subclone number correct", hue = "pipes", data = df, estimator = np.sum, 
                    palette = sns.hls_palette(8, l=.5, s=.8), errwidth = 0);
plt.xlabel("Phylogeny index");
plt.title("Number of succesful runs and correct estimations of subclone number of each tool (pipe) per simulation kind (phylogeny)");

#%% Number of succesful runs and correct estimations of subclone number of pipes per parameter
ax[0] = sns.barplot(x="pars", y="estimation succesful", hue = "pipes", data = df, estimator = np.sum, 
                    palette = sns.hls_palette(8, l=.3, s=.8), errwidth = 0);
ax[0] = sns.barplot(x="pars", y="subclone number correct", hue = "pipes", data = df, estimator = np.sum, 
                    palette = sns.hls_palette(8, l=.5, s=.8), errwidth = 0);
plt.xlabel(parameter);
plt.title("Number of succesful runs and correct estimations of subclone number of each tool (pipe) per simulation parameter (sample number)");


#%%=============================================================================
# expand the ccfs
# =============================================================================

#expand the ccfs for multiple subclones
def expand_ccfs(df, colname, recursive = False, add_tag = None, not_index = None, not_inner_index = None):
    ccfs = pd.DataFrame(df[colname].values.tolist(), index = df.index);
    
    for i in range(len(ccfs.columns)):
        if add_tag != None:
            new_colname = colname + add_tag + "_" + str(i);
        else:
            new_colname = colname + "_" + str(i);
            
        ccfs = ccfs.rename({i : new_colname}, axis = 1);
        
        if not_index == i:
            # print("drop:",new_colname);
            ccfs = ccfs.drop(labels = [new_colname], axis = 1);
            
        elif recursive == True:
            ccfs = expand_ccfs(ccfs, new_colname, not_index = not_inner_index);

    df = df.drop(labels = [colname], axis = 1);
    df = pd.concat((df[:],ccfs[:]), axis = 1);
    return(df);

#%% Custom for unadjusted (pre-ISAfix dataset)
#create temporalily modified dataframe with correct subclone numbers estimated

#for the 1 sample case: only show ccfs of last sample
df_mod = df.head(900);
df_mod = df_mod[df_mod["subclone number correct"] == 1];
df_mod = df_mod.drop(labels = ["subclone number true","run time"], axis = 1);

df_mod = expand_ccfs(df_mod, "ccfs true", recursive = True, not_inner_index = 0);
df_mod = expand_ccfs(df_mod, "ccfs", recursive = True, add_tag = " estimate");

#for the 1+ samples
df_mod2 = df.tail(-900);
df_mod2 = df_mod2[df_mod2["subclone number correct"] == 1];
df_mod2 = df_mod2.drop(labels = ["subclone number true","run time"], axis = 1);

df_mod2 = expand_ccfs(df_mod2, "ccfs true", recursive = True);
df_mod2 = expand_ccfs(df_mod2, "ccfs", recursive = True, add_tag = " estimate");

df_mod = pd.concat((df_mod,df_mod2));

#multiply sci_clone estimates by two, to correct
sci_ccfs = [col for col in df_mod[df_mod["pipes"] == "sciclone"] if col.startswith("ccfs estimate")];
df_mod[sci_ccfs] *= 2;

# df_mod = expand_ccfs(df_mod, "ccfs", recursive = True, add_tag = " estimate");
# df_mod = expand_ccfs(df_mod, "ccfs true", recursive = True);

#%% ISAfixed dataset

df_mod = df[df["subclone number correct"] == 1];
# df = df.drop(labels = ["subclone number true","run time"], axis = 1);

df_mod = expand_ccfs(df_mod, "ccfs true", recursive = True);
df_mod = expand_ccfs(df_mod, "ccfs", recursive = True, add_tag = " estimate");

#%%
def compare_ccfs(df, true_str, estimate_str):
    t_ccfs = [col for col in df if col.startswith(true_str)];
    e_ccfs = [col for col in df if col.startswith(estimate_str)];
    
    t_arr = df[t_ccfs].to_numpy();
    e_arr = df[e_ccfs].to_numpy();
    
    diff_ccfs = pd.DataFrame(e_arr - t_arr, columns = df[e_ccfs].columns);
    
    return(diff_ccfs);

diff_ccfs = compare_ccfs(df_mod, "ccfs true", "ccfs estimate");
diff_ccfs = diff_ccfs.abs().sum(axis = 1);
df_mod["ccfs error"] = diff_ccfs;



