# -*- coding: utf-8 -*-
"""
Created on Wed May 20 14:34:31 2020

@author: niels
"""

import numpy as np;
import pandas as pd;

# =============================================================================
# specify input
# =============================================================================
cohort_tags = [""];

meta_parameter = "sample number";
parameter = "mcmc samples";
meta_parameter_scaling = lambda x : x + 1;
parameter_scaling = lambda x : 2500#int(25 + 243.75*x);#round(0.001 + 0.1*x,4);

SINGLE = 1;

pipe_names = {
    0 : "phylowgs",
    # 1 : "sciclone",
    1 : "dpclust",
    };

data_name = "mcmcrun_2500";#"pwgs-sci-dpc_it25_50s_6l_2000g_0_4_4n_scifix";
data_path = "C:\\Users\\niels\\Documents\\Nanobiology\\BEP\\data\\"+data_name+"\\";
data_files = [[f"SAMPLE_MCMC_s{i}{cohort_tags[cohort]}\\"
              for i in range(1,5)] for cohort in range(len(cohort_tags))];

data_names = ["pipe_estimations.npy","true_values.npy"];

# =============================================================================
# create dataframe functions
# =============================================================================

#process the raw data
def preprocessData(e,t):
    def collapse_to_frame(a, names = None):
        dims = np.array(a.shape);
        datapoints = a.reshape(np.prod(dims[0:-1]),-1);
        
        prev_rep = 1;
        all_columns = datapoints
        for i in reversed(range(np.ndim(a)-1)):
            # print("i:",i);
            rep = prev_rep;
            # print("rep:",rep);
            rep_arr = np.repeat(np.arange(dims[i]), rep);
            # print("rep_arr:",rep_arr);
            # print("datapoints.shape[0]:",datapoints.shape[0]);
            # print("len(rep_arr)",len(rep_arr));
            new_column = np.array( list(rep_arr) * int(datapoints.shape[0]//len(rep_arr)) );
            # print("new_column:",new_column);
            all_columns = np.column_stack((new_column,all_columns))
            prev_rep = len(rep_arr);
        
        if names != None:
            df = pd.DataFrame(all_columns, columns = names);
        else:
            df = pd.DataFrame(all_columns);
        
        return (df);
    
    # =============================================================================
    # merge true data with the estimated data in dataframe
    # =============================================================================
    
    #expand the true values to fit the estimations dataframe
    t_expand = np.empty_like(e[:,:,:,:,:-1]);
    
    for i in range(e.shape[0]):
        for j in range(e.shape[1]):
            t_expand[i,j,:,:,:] = np.full((e.shape[2],e.shape[3],2),t[i,j,:]);
    
    df_e = collapse_to_frame(e, names = ["pars","loops","pipes","subloops","subclone number","ccfs","run time"]);
    df_t = collapse_to_frame(t_expand, names = ["pars","loops","pipes","subloops","subclone number true","ccfs true"]);
    
    #merge
    df = pd.merge(df_e,df_t,how = "left", on = ["pars","loops","pipes","subloops"]);
    
    for cat in ["pars","loops","pipes","subloops","subclone number","subclone number true"]:
        df[cat] = df[cat].astype(int);
    df["run time"] = df["run time"].astype(float);
    
    df["pipes"] = df["pipes"].replace(pipe_names.keys(),pipe_names.values());
    
    df["pars"] = df["pars"].apply(parameter_scaling)
    
    #tag rows with correct subclone number estimate
    df["subclone number correct"] = (df["subclone number"] == df["subclone number true"]).astype(int);
    df["estimation succesful"] = (df["subclone number"] != 0).astype(int);
    
    return(df);

#expand the ccfs for multiple subclones
def expand_ccfs(df, colname, recursive = False, add_tag = None, not_index = None, not_inner_index = None):
    ccfs = pd.DataFrame(df[colname].values.tolist(), index = df.index);
    
    for i in range(len(ccfs.columns)):
        if add_tag != None:
            new_colname = colname + add_tag + "_" + str(i);
        else:
            new_colname = colname + "_" + str(i);
            
        ccfs = ccfs.rename({i : new_colname}, axis = 1);
        
        if not_index == i:
            # print("drop:",new_colname);
            ccfs = ccfs.drop(labels = [new_colname], axis = 1);
            
        elif recursive == True:
            ccfs = expand_ccfs(ccfs, new_colname, not_index = not_inner_index);

    df = df.drop(labels = [colname], axis = 1);
    df = pd.concat((df[:],ccfs[:]), axis = 1);
    return(df);

#add ccfs errors
def compare_ccfs(df, true_str, estimate_str):
    t_ccfs = [col for col in df if col.startswith(true_str)];
    e_ccfs = [col for col in df if col.startswith(estimate_str)];
    
    t_arr = df[t_ccfs].to_numpy();
    e_arr = df[e_ccfs].to_numpy();
    
    diff_ccfs = pd.DataFrame(e_arr - t_arr, columns = df[e_ccfs].columns, index = df.index);
    
    return(diff_ccfs);

#create all the dataframes and tag with metaparameter
def dfsConstruct():
    if SINGLE:
        e = np.load(data_path + data_names[0], allow_pickle = True);
        t = np.load(data_path + data_names[1], allow_pickle = True);
        
        df = preprocessData(e,t);
        
        df["loops"] = df["loops"]//6;
    else:
        dfs = [];
        
        for data_file in data_files[cohort]:
            e = np.load(data_path + data_file + data_names[0], allow_pickle = True);
            t = np.load(data_path + data_file + data_names[1], allow_pickle = True);
            
            df = preprocessData(e,t);
            df["metapars"] = meta_parameter_scaling(data_files[cohort].index(data_file));
            
            dfs.append(df);
        
        #concatenate the dataframes (vertically)
        df = pd.concat(dfs);
    
    print("df made: contains all estimations and true values")
    
    df_mod = df[df["subclone number correct"] == 1];
    # df = df.drop(labels = ["subclone number true","run time"], axis = 1);
    
    df_mod = expand_ccfs(df_mod, "ccfs true", recursive = True);
    df_mod = expand_ccfs(df_mod, "ccfs", recursive = True, add_tag = " estimate");
    
    
    diff_ccfs = compare_ccfs(df_mod, "ccfs true", "ccfs estimate");
    diff_ccfs = diff_ccfs.abs().sum(axis = 1);
    df_mod["ccfs error"] = diff_ccfs;
    
    print("df_mod made: contains expanded ccfs of correct subclone number estimations")
    
    if not SINGLE and meta_parameter == "sample number":
        print("normalize ccfs error over sample number",df_mod["metapars"].unique());
        df_mod["ccfs error"] = df_mod["ccfs error"]/df_mod["metapars"];
    
    return(df,df_mod);

# =============================================================================
# Make dataframes
# =============================================================================

#if multiple cohorts
if len(cohort_tags) > 0:
    cohort_df = [];
    cohort_df_mod = [];
    
    for cohort in range(len(cohort_tags)):
        print(f"Process cohort {cohort}");
        
        df,df_mod = dfsConstruct();
        
        df["cohort"] = cohort;
        
        cohort_df.append(df);
        cohort_df_mod.append(df_mod);
    
    df = pd.concat(cohort_df);
    df_mod = pd.concat(cohort_df_mod);
    
    print("All cohort dfs have been combined")

#if single dataset
if SINGLE and len(cohort_tags) == 0:
    if "parameter" not in globals():
        parameter = meta_parameter;
        parameter_scaling = meta_parameter_scaling;
    
    df,df_mod = dfsConstruct();
        
    if parameter == "sample number":
        df_mod["ccfs error"] = df_mod["ccfs error"]/df_mod["pars"];

# translate labels
def tl(prev,new,dataframes=[df,df_mod]):
    for df in dataframes:
        if prev in df.columns:
            df[new] = df[prev];
            del df[prev];

labels = {
    "pars" : parameter,
    "metapars" : meta_parameter,
    "pipes" : "tool",
    "loops" : "phylogeny",
    "subclone number correct" : "correctness",
    };

for label in labels.keys():
    tl(label,labels[label]);

#save output
if input("Save - type 'y'") == 'y':
    # name = input("Name: ");
    df.to_pickle(data_path+data_name);
    df_mod.to_pickle(data_path+data_name+"mod");