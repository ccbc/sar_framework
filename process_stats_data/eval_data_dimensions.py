# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 15:50:29 2020

@author: niels
"""
import pandas as pd;

data_name = "PWGSFIX_4_samples";
data_path = "C:\\Users\\niels\\Documents\\Nanobiology\\BEP\\data\\"+data_name+"\\";

name = data_path+data_name;
df = pd.read_pickle(name);
df_mod = pd.read_pickle(name+"mod");

par = "mutation load"
metapar = "sample number"

attributes = [
    "cohort",
    "tool",
    "phylogeny",
    "subloops",
    par,
    metapar,
    ]

for attr in attributes: print("{:15}: {:>5}".format(attr,len(df[attr].unique())))
print()
print(par,"\n",df[par].unique())
print(metapar,"\n",df[metapar].unique())

