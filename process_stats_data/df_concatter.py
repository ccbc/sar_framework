# -*- coding: utf-8 -*-
"""
Created on Sun Jul  5 18:51:22 2020

@author: niels
"""
import pandas as pd;

data_name = "mcmc_run_2";
data_path = "C:\\Users\\niels\\Documents\\Nanobiology\\BEP\\data\\"+data_name+"\\";

name = data_path+data_name;
df = pd.read_pickle(name);
df_mod = pd.read_pickle(name+"mod");

data_name2 = "mcmcrun_2500";
####################################HERE THE OUTPUT IS SAVED
data_path2 = "C:\\Users\\niels\\Documents\\Nanobiology\\BEP\\data\\"+data_name2+"\\";

name2 = data_path2+data_name2;
df2 = pd.read_pickle(name2);
df_mod2 = pd.read_pickle(name2+"mod");

new_df = pd.concat([df,df2]);
new_df_mod = pd.concat([df_mod,df_mod2]);

#save output
if input("Save - type 'y'") == 'y':
    # name = input("Name: ");
    new_df.to_pickle(data_path2+data_name2);
    new_df_mod.to_pickle(data_path2+data_name2+"mod");