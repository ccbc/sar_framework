# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 13:39:37 2020

@author: niels
"""
# import numpy as np;
import pandas as pd;
import seaborn as sns; sns.set(font_scale=2, style="whitegrid")
import matplotlib.pyplot as plt;
# from pylab import savefig;

data_name = "mcmc_run_2";
data_path = "C:\\Users\\niels\\Documents\\Nanobiology\\BEP\\data\\"+data_name+"\\";

outdir = data_path;
# SAVE = 1;

name = data_path+data_name;
df = pd.read_pickle(name);
df_mod = pd.read_pickle(name+"mod");

meta_parameter = "sample number";
parameter = "mutation load";

# pal = [sns.color_palette("Paired")[i] for i in [1,3,5,7,9]]
pal = sns.color_palette("coolwarm", 5);
hue = "mutation load";
style = None;
x = "run time";
y = "ccfs error";
df_i = df_mod
xlim = [10,1000];
ylim = [.01,5];

xscale="log";
yscale="log";

# df_i["category"] = "sample number = " + df_i["sample number"].astype(str) +\
#                   ", correctness = " + df_i["correctness"].astype(str);
hue = "mcmc samples";
style = None;   

# pal = [sns.color_palette("Paired")[i] for i in [1,7]]

mask = df_i["tool"] == "dpclust";
plt.figure(figsize=(10.5,8.25));
fig = sns.scatterplot(x,y, hue = hue, style = style, data = df_i[mask], palette = pal);
fig.set(xscale=xscale, yscale=yscale, xlim=xlim, ylim=ylim);
fig.legend(loc = 3)
fig.set_title("The run time vs the ccfs error for \n DPClust,  seperated for different "+hue+"s taken          ")
plt.show();

# pal = [sns.color_palette("Paired")[i] for i in [1,3,5,7]]

mask = df_i["tool"] == "phylowgs";
plt.figure(figsize=(10.5,8.25));
fig = sns.scatterplot(x,y, hue = hue, style = style, data = df_i[mask], palette = pal);
fig.set(xscale=xscale, yscale=yscale, xlim=xlim, ylim=ylim);
fig.legend(loc = 3)
fig.set_title("The run time vs the ccfs error for \nPhyloWGS, seperated for different "+hue+"s taken          ")
plt.show();

# mask = df_mod["tool"] == "sciclone";
# fig = sns.scatterplot("run time","correctness", hue = hue, data = df[mask], palette = pal);
# fig.set(xscale="log", yscale="log", xlim=[2,500], ylim = [0.01,3])
# fig.set_title("The run time and ccfs error of SciClone, seperated for different sample numbers")
# plt.show();
