# -*- coding: utf-8 -*-
"""
Created on Wed May 20 16:33:12 2020

@author: niels
"""

import numpy as np;
import pandas as pd;
import seaborn as sns; sns.set(font_scale=2, style="whitegrid")
import matplotlib.pyplot as plt;
from pylab import savefig;


data_name = "mcmcrun_2500";
data_path = "C:\\Users\\niels\\Documents\\Nanobiology\\BEP\\data\\"+data_name+"\\";

outdir = data_path;
SAVE = 1;
SINGLE = 1;
bar_counterparts = 0;

name = data_path+data_name;
df = pd.read_pickle(name);
df_mod = pd.read_pickle(name+"mod");

meta_parameter = "sample number";
parameter = "mcmc samples";

xpars = [parameter,meta_parameter];

estimator_dict = {
    "sum" : "Summed",
    "mean" : "Mean",
    }

ylim = {
        "ccfs error": (0,2.5),
        "correctness": (0,.6),
        "subclone number": (0,6),
        "run time": (0,300),
        }

def plotStat(xpar,ypar,huepar = None,estimatorpar = "mean",df = df, ypar2 = None,
             ypar2_label = "True", ypar2_hue = None, stylepar = None, ylim = ylim,
             plottype = "line", ylabel = None, legend = None):
    
    plt.figure(figsize=(10,8));
    
    if plottype == "line":
        if stylepar != None:
            sns.lineplot(x=xpar, y=ypar, hue=huepar, style=stylepar, estimator=estimatorpar, data=df);
        else:
            sns.lineplot(x=xpar, y=ypar, hue=huepar, estimator=estimatorpar, data=df);
            
            
        if ypar2 != None:
            if ypar2_hue != None:
                sns.lineplot(x=xpar, y=ypar2, hue=ypar2_hue, color = "red", label = ypar2_label, data=df);
            else:
                sns.lineplot(x=xpar, y=ypar2, color = "red", label = ypar2_label, data=df);
        
        particks = df[xpar].unique();
        parlabels = None;
        # parlabels = sum([[str(particks[::4][i])] + [""]*(4-1) for i in range(len(particks)//4+1)],[]);
        plt.xticks(particks,parlabels);
        
        if ylim != None:
            plt.ylim(ylim[ypar]);
    
    elif plottype == "bar":
        estimatorpar = "sum";
        
        if stylepar != None:
            sns.barplot(x=xpar, y=ypar, hue=huepar, data=df, estimator=np.sum, 
                        palette = sns.hls_palette(8, l=.3, s=.8), errwidth = 0);
        else:
            sns.barplot(x=xpar, y=ypar, hue=huepar, data=df, estimator=np.sum, 
                        palette = sns.hls_palette(8, l=.3, s=.8), errwidth = 0);

            
        if ypar2 != None:
            if ypar2_hue != None:
                sns.barplot(x=xpar, y=ypar2, hue=huepar, data=df, estimator=np.sum,
                            palette = sns.hls_palette(8, l=.5, s=.8), errwidth = 0);
            else:
                sns.barplot(x=xpar, y=ypar2, data=df, estimator=np.sum,
                            palette = sns.hls_palette(8, l=.5, s=.8), errwidth = 0);
    
    if legend != None:
        plt.legend(legend);
    
    plt.xlabel(xpar);
    if ylabel != None:
        plt.ylabel(ylabel);
    
    if ypar2 != None:
        ypar2_title = " with " + ypar2;
    else:
        ypar2_title = "";
    
    if huepar != None:
        hue_title = " for each " + huepar;
        hue_name = "_" + huepar;
    else:
        hue_name = "";
        hue_title = "";
        
    plt.title(estimator_dict[estimatorpar] +
              " " +
              ypar + #ypar2_title +
              " of estimations" + 
              hue_title +
              " vs simulation parameter - " +
              xpar
              , wrap=True);
    
    if SAVE: savefig(outdir+xpar + hue_name + "_" + ypar + ".png");
    plt.clf();

for xpar in xpars:
    plotStat(xpar, "ccfs error", "tool", df = df_mod);
    plotStat(xpar, "ccfs error", "phylogeny", df = df_mod, legend = "012345");
    
    
    plotStat(xpar, "subclone number", "tool", ypar2 = "subclone number true",
              ylim = ylim, ylabel = "subclone number estimated");
    plotStat(xpar, "correctness", "tool");
    plotStat(xpar, "correctness", "phylogeny", legend = "012345");
    
    
    plotStat(xpar, "run time", "tool", stylepar = "correctness");
    plotStat(xpar, "run time", "phylogeny", legend = "012345");
    
    
    if bar_counterparts:
        plotStat(xpar, "ccfs error", "tool", df = df_mod, plottype = "bar");
        plotStat(xpar, "ccfs error", "phylogeny", df = df_mod, plottype = "bar");
        
        plotStat(xpar, "subclone number", "tool", ypar2 = "subclone number true",
                  ylabel = "subclone number estimated", plottype = "bar");
        plotStat(xpar, "correctness", "tool", plottype = "bar");
        
        plotStat(xpar, "run time", "tool", stylepar = "correctness", plottype = "bar");
        plotStat(xpar, "run time", "phylogeny", plottype = "bar");
    
    plotStat(xpar, "estimation succesful", "tool", 
              ypar2 = "correctness", ypar2_hue = "tool", ypar2_label = "correct",
              plottype = "bar");
    pass;
        
    
plotStat("phylogeny", "estimation succesful", ypar2 = "correctness",
          ypar2_label = "correct", plottype = "bar");
plotStat("phylogeny", "run time", plottype = "bar");
plotStat("phylogeny", "ccfs error", ypar2 = "correctness", df = df_mod,
          ypar2_label = "correct", plottype = "bar");
    
