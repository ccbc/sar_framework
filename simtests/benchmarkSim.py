import sys;
rrg_path = "/home/nburghoorn/userData/rrg/randomreadgenerator/";
sys.path.append(rrg_path);

import settings;
from main import main;

import numpy as np;
from time import time;

setts = settings.settings();

genome_sizes = np.logspace(1,4,10).astype(int);
pop_sizes = np.logspace(1,3,5).astype(int);

times = np.zeros((len(genome_sizes),len(pop_sizes)))

for g in range(len(genome_sizes)):
  print(g, file = sys.__stdout__)
  for p in range(len(pop_sizes)):
    setts.rrg["nuclN"] = genome_sizes[g];
    setts.rrg["cellN"] = pop_sizes[p];
    
    t = time();
    main(setts);
    times[g,p] = time()-t;

print("g:");
print(genome_sizes.tolist());

print("p:");
print(pop_sizes.tolist());

print("times:");
print(times.tolist(), file = sys.__stdout__);
  
  
