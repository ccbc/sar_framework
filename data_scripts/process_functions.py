import numpy as np;
import matplotlib;
matplotlib.use("Agg");
import matplotlib.pyplot as plt;
from pylab import savefig;
  
def get_correct_sc_n_number(d):
  e_correct_sc_n = get_correct_sc_n_estimates(d);
  number_correct = np.size(e_correct_sc_n, axis = 3);
  
  return(number_correct);
  
def get_correct_sc_n_per_pipe(d):
  e_correct_sc_n = get_correct_sc_n_estimates(d);
  number_per_pipe = np.sum(e_correct_sc_n, axis = 1);
  
  return(number_per_pipe);

def get_correct_sc_n_per_pipe(d):
  e_correct_sc_n = get_correct_sc_n_estimates(d);
  number_per_parpipe = np.sum(np.array(e_correct_sc_n, dtype = int),axis = 3);
  
  return(number_per_parpipe);

class data:
  
  dims = dict(
    pars = 0,
    loops = 1,
    pipes = 2,
    subloops = 3,
    datapoints = 4,
    
    subclone_number = 0,
    ccfs = 1,
    run_times = 2,
  )
  
  labels = {
    0 : 'pars',
    1 : 'loops',
    2 : 'pipes',
    3 : 'subloops',
    4 : 'datapoints',
  }
  
  dims_left = [0,1,2,3,4];
  
  def __init__(self,e,t):
    self.e = e; #estimated data
    self.e_backup = np.copy(e); #to restore data
    self.t = t; #true values
    self.PARS,self.LOOPS,self.PIPES,self.SUBLOOPS,self.DATAPOINTS = e.shape;
    self.pars = list(range(self.PARS));
    self.loops = list(range(self.LOOPS));
    self.pipes = list(range(self.PIPES));
    self.subloops = list(range(self.SUBLOOPS));
    self.datapoints = list(range(self.DATAPOINTS));
    
  #showing properties
  
  def print_dims(self):
    print("pars:",self.PARS,"\nloops:",self.LOOPS,"\npipes:",self.PIPES,"\nsubloops:",self.SUBLOOPS,"\ndatapoints:",self.DATAPOINTS)

  def print_e(self):
    print(self.e);

  #subclone_number analysis

  def wrong_subclone_number_to_zero(self):
    #extract the subclone numbers from the data
    t_sc_n = self.t[:,:,0];
    e_sc_n = self.e[:,:,:,:];
    
    #expand the true data to fit the estimated data
    t_compare = np.empty_like(e_sc_n);
    for i in self.pars:
      for j in self.loops:
        t_compare[i,j,:,:] = np.full((self.PIPES,self.SUBLOOPS),t_sc_n[i,j]);
    
    e_wrong_sc_n = (e_sc_n != t_compare);
    
    self.e[e_wrong_sc_n] = 0;

  def correct_subclone_number_to_one(self):
    
    self.extract("subclone_number");
    
    #extract the subclone numbers from the data
    t_sc_n = self.t[:,:,0];
    e_sc_n = self.e[:,:,:,:];
    
    #expand the true data to fit the estimated data
    t_compare = np.empty_like(e_sc_n);
    for i in self.pars:
      for j in self.loops:
        t_compare[i,j,:,:] = np.full((self.PIPES,self.SUBLOOPS),t_sc_n[i,j]);
    
    e_correct_sc_n = (e_sc_n == t_compare);
    
    self.e[e_correct_sc_n] = 1;
    
  def only_n_subclone_number_to_one(self, n):
    
    self.extract("subclone_number");
    
    #expand the mask to fit the estimated data
    mask = np.full_like(self.e,n);
    
    #set matches to one
    #set clashing to zero
    e_match_sc_n = (self.e == mask);
    e_wrong_sc_n = (self.e != mask);
    self.e[e_match_sc_n] = 1;
    self.e[e_wrong_sc_n] = 0;
    
  def above_n_subclone_number_to_one(self, n, equal = True):
    
    self.extract("subclone_number");
    
    if equal:
      n -= 1;
    
    #expand the mask to fit the estimated data
    mask = np.full_like(self.e,n);
    
    #set matches to one
    #set clashing to zero
    e_match_sc_n = (self.e > mask);
    e_wrong_sc_n = (self.e <= mask);
    self.e[e_match_sc_n] = 1;
    self.e[e_wrong_sc_n] = 0;
    
  def below_n_subclone_number_to_one(self, n, equal = True):
    
    self.extract("subclone_number");
    
    if equal:
      n += 1;
    
    #expand the mask to fit the estimated data
    mask = np.full_like(self.e,n);
    
    #set matches to one
    #set clashing to zero
    e_match_sc_n = (self.e < mask);
    e_wrong_sc_n = (self.e >= mask);
    self.e[e_match_sc_n] = 1;
    self.e[e_wrong_sc_n] = 0;
    
  def interval_subclone_number_to_one(self, nlow, nhigh, equal = True, outside = False):
    if outside:
      print("not implemented yet");
      return();
      
    self.above_n_subclone_number_to_one(nlow, equal = equal);
    a = np.copy(self.e);
    self.restore();
    
    self.below_n_subclone_number_to_one(nhigh, equal = equal);
    b = np.copy(self.e);
    self.restore();
    
    self.e = np.logical_and(a,b);
    
  #fundamental fuctions  
  
  def sum_dims(self, dims = [], restore = False, ret = False):
    if len(dims) != 0:
      for dim in dims:
        self.sum(dim);
    
    if ret:
      result = self.e;
    
    if restore:
      self.restore();
    
    if ret:
      return(result);

  def sum(self,dim):

    self.e = np.sum(self.e, axis = self.dims[dim]);
    self.dims_left.remove(self.dims[dim]);
  
  def mean(self,dim):
    
    self.e = np.mean(self.e, axis = self.dims[dim]);
    self.dims_left.remove(self.dims[dim]);
    
  def extract(self,datapoint):
    
    indices = np.zeros(self.e.shape[-1]);
    indices[self.dims[datapoint]] = 1;
    self.e = np.compress(indices, self.e, axis = np.ndim(self.e)-1);
    self.e = np.squeeze(self.e, axis = np.ndim(self.e)-1);
    
  def restore(self):
    self.e = np.copy(self.e_backup);
    self.dims_left = [0,1,2,3,4];
  
  def plot(self, title = "", xlabel = "", ylabel = "", legend = [], save = "tempfig.png", transpose = False):
    if xlabel == "":
      xlabel = self.labels[self.dims_left[0]];
    if ylabel == "":
      ylabel = self.labels[self.dims_left[1]];
    
    if transpose:
      plt.plot(self.e.T);
    else:
      plt.plot(self.e);
    plt.title(title);
    plt.xlabel(xlabel);
    plt.ylabel(ylabel);
    
    if len(legend) != 0:
      plt.legend(legend);
    
    if save != "":
      savefig(save);
