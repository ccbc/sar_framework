# -*- coding: utf-8 -*-
"""
Created on Wed May  6 16:16:45 2020

@author: niels
"""
import numpy as np;
import matplotlib;
matplotlib.use("Agg");
import matplotlib.pyplot as plt;
from pylab import savefig;

K = 0; #number of subclones
M = 2000; #number of loci
p = 0.05; #chance of mutation on single locus

def calculate_properties(K,M,p):
    chance_0_mut_on_locus = (1-p)**(K);
    chance_1_mut_on_locus = K*p*((1-p)**(K-1));
   
    # print("P(Zero mutations on single locus) = ",chance_0_mut_on_locus);
    # print("P(One mutation on single locus) = ",chance_1_mut_on_locus);
   
    chance_1orless_mut_on_locus = chance_0_mut_on_locus + chance_1_mut_on_locus
   
    # print("P(Not more that 1 mutation on a single locus) = ",chance_1orless_mut_on_locus);
   
    P_isa = chance_1orless_mut_on_locus**M
   
    # print("P(No ISA violation) = ",P_isa);
   
    mut_mass = M*p;
   
    # print("Mutation mass = ", mut_mass);
   
    return(P_isa,mut_mass);

font = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 10,
        }

p_value_ISA = 1 - 0.05;

k_range = np.arange(1,6)
p_resol = 100;

plt.figure(figsize=(10,8));

plt.subplot(221);
p_range = np.linspace(0.001,0.1,p_resol);
for k in k_range:
    isa_probabilities,_ = calculate_properties(k, M, p_range);
    plt.plot(p_range, isa_probabilities);
plt.hlines(.95,p_range[0],p_range[-1]);
plt.xlim([0,0.030]);
plt.ylim([0.4,1.1]);
plt.xlabel("mutation chance");
plt.ylabel("probability");


plt.legend(k_range);
plt.title("Probabilities for no ISA violation for different \n number of subclones and different mutation chances", fontdict = font);
plt.annotate("a",xy=(-.2,1.2),xycoords="axes fraction",size=14,ha="center",va="bottom");

# plt.subplot(232);
# p_range = np.linspace(0.001,0.01,p_resol);
# for k in k_range:
#     isa_probabilities,_ = calculate_properties(k, M, p_range);
#     plt.plot(p_range, isa_probabilities);
# plt.hlines(.95,p_range[0],p_range[-1]);
# plt.legend(k_range);
# plt.title("Probabilities for no ISA violation\n for different #subclones and\n different mutation chances", fontdict = font);
# 
# 
#A high mutation mass is favourable for subclonal architecture reconstruction
p_resol = 1000;

plt.subplot(222);
p_range = np.linspace(0.001,1,p_resol);
max_mut_masses = [];
max_mut_masses_indices = [];
k_range = k_range[1:]
for k in k_range:
    isa_probabilities,mutation_mass = calculate_properties(k, M, p_range);

    #calculate the highest mutation mass above the isa p-value
    highest_mut_mass_index = np.argmax(mutation_mass[np.where(isa_probabilities > p_value_ISA)]);
    max_mut_masses.append(mutation_mass[highest_mut_mass_index]);
    max_mut_masses_indices.append(highest_mut_mass_index);


plt.plot(k_range, max_mut_masses);

plt.xlabel("subclone number");
plt.ylabel("highest mutation load");
plt.xticks(k_range);

p_legend = list(map((lambda x: str(round(x,3))),p_range[max_mut_masses_indices]));
k_legend = list(map((lambda x : " - " + str(x)),k_range));
pk_legend = ", ".join(list(map(str.__add__,p_legend,k_legend)));

plt.title(f"Highest mutation load for P(no ISA violation) > {p_value_ISA}\n for different number of subclones and different mutation chances", fontdict = font);
plt.annotate("b",xy=(-.2,1.2),xycoords="axes fraction",size=14,ha="center",va="bottom");

#fix subclone number chance the genome size M
K_range = np.arange(2,6);
K = 2;
m_range = np.arange(1000,5000,1000);

# presol = 100;
# 
# plt.subplot(234);
# p_range = np.linspace(0.001,1,p_resol);
# for m in m_range:
#     isa_probabilities,_ = calculate_properties(K, m, p_range);
#     plt.plot(p_range, isa_probabilities);
# plt.hlines(.95,p_range[0],p_range[-1]);
# plt.legend(m_range);
# plt.title(f"K = {K}\nProbabilities for no ISA violation\n for different genome sizes and\n different mutation chances", fontdict = font);


plt.subplot(223);
p_range = np.linspace(0.001,0.03,p_resol);
for m in m_range:
    isa_probabilities,_ = calculate_properties(K, m, p_range);
    plt.plot(p_range, isa_probabilities);

plt.xlim([0,0.030]);
plt.ylim([0.4,1.1]);
plt.xlabel("mutation chance");
plt.ylabel("probability")

plt.hlines(.95,p_range[0],p_range[-1]);
plt.legend(m_range);
plt.title(f"Probabilities for no ISA violation for different\n genome sizes and different mutation chances", fontdict = font);
plt.annotate("c",xy=(-.2,1.2),xycoords="axes fraction",size=14,ha="center",va="bottom");

#A high mutation mass is favourable for subclonal architecture reconstruction

plt.subplot(224);

for K in K_range:
  m_range = np.linspace(1000,100000,20);
  p_resol = 10000;

  p_range = np.linspace(0.0001,0.01,p_resol);

  max_mut_masses = [];
  max_mut_masses_indices = [];
  for m in m_range:
      isa_probabilities,mutation_mass = calculate_properties(K, m, p_range);

      #calculate the highest mutation mass above the isa p-value
      highest_mut_mass_index = np.argmax(mutation_mass[np.where(isa_probabilities > p_value_ISA)]);
      # print("Points ISA: ",isa_probabilities);
      # print("Points above ISA pvalue: ",np.where(isa_probabilities > p_value_ISA));
      # print(p_range[highest_mut_mass_index]);
      max_mut_masses.append(mutation_mass[highest_mut_mass_index]);
      max_mut_masses_indices.append(highest_mut_mass_index);


  plt.plot(m_range, max_mut_masses);

  p_legend = list(map((lambda x: str(round(x,10))),p_range[max_mut_masses_indices]));
  m_legend = list(map((lambda x : " - " + str(round(x))),m_range));

pm_legend = ", \n".join(list(map(str.__add__,p_legend,m_legend)));
plt.legend(K_range);
plt.title(f"Highest mutation load for P(no ISA violation) > {p_value_ISA}\n for different genome sizes and different mutation chances", fontdict = font);

plt.xlabel("genome size");
plt.ylabel("highest mutation load");
plt.annotate("d",xy=(-.2,1.2),xycoords="axes fraction",size=14,ha="center",va="bottom");

plt.tight_layout(pad = 3);
# savefig(f"mut_load_ISA_{p_value_ISA}.png");
savefig(f"ISA_highest_mutload_{p_value_ISA}.png",bbox_inches="tight");


