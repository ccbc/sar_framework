#installation

import os;
import fileinput;

#get all the path specifications
sar_path = os.path.dirname(os.path.realpath(__file__));
dir_path = "/".join(sar_path.split("/")[:-1]) + "/";

#rename to "rrg"
os.system("mv "+sar_path+" "+dir_path+"rrg")

sar_path = dir_path + "rrg" + "/";
rrg_path = sar_path + "randomreadgenerator/";

print("Current installation path: ", sar_path);

old_path = "/mnt/data/ccbc_environment/users/nburghoorn/";
new_path = dir_path;

if not new_path[-1] == "/":
  new_path = new_path + "/";

files = [
  rrg_path+"simulation_report.py",
  rrg_path+"ui.py",
  rrg_path+"main.py",
  rrg_path+"settings.py",
  rrg_path+"fishPlot.R",
  rrg_path+"PlotVAFHist.R",
  rrg_path+"programpipes/getPaths.py",
  rrg_path+"programpipes/install_phylowgs.sh",
  rrg_path+"programpipes/install_phylowgs_parallel.sh",
  rrg_path+"programpipes/phylowgs_results_make.sh",
  rrg_path+"programpipes/phylowgs_results_make_parallel.sh",
  rrg_path+"programpipes/sciclone_pipeline.R",
  rrg_path+"programpipes/quantumclone_pipeline.R",
  rrg_path+"stats_files/statsrun_export.sh",
  rrg_path+"stats_files/statsrun_script.sh",
]

replace_text = {
  old_path : new_path
}

for file in files:
  with open(file, 'r') as f :
    filedata = f.read();

  # Replace the target string
  for key,val in replace_text.items():
    filedata = filedata.replace(key, val);

  # Write the file out again
  with open(file, 'w') as f:
    f.write(filedata);

sof_path = dir_path+"software";

if not os.path.isdir(sof_path):
  print("Made software folder at " + sof_path);
  os.mkdir(sof_path);

print(sof_path);

pwgs_number = int(input("Enter the number of PhyloWGS installations (horizontally installed for parallel execution):\n"));

print("Make the PhyloWGS installations");
os.system(rrg_path+"/programpipes/install_phylowgs.sh");
os.system(rrg_path+"/programpipes/install_phylowgs_parallel.sh "+str(pwgs_number));
